import axios from "axios";

const $axios = axios.create({
    baseURL: process.env.VUE_APP_API_URL,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
    },
});

$axios.defaults.timeout = 10000;
$axios.interceptors.request.use(
    (config) => {
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);
$axios.interceptors.response.use(
    (response) => {
        return Promise.resolve(response.data);
    },
    (error) => {
        if (error.response.status) {
            return Promise.reject(error.response);
        }
    }
);

export default $axios;
