import Vue from "vue";
import Vuex from "vuex";
import $api from "../services/api";
import { User } from "../types/user";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        listUser: [],
        amountUser: 0,
    },
    getters: {
        user: (state) => {
            return { listUser: state.listUser, amountUser: state.amountUser };
        },
    },
    mutations: {
        setListUser(state, user: []) {
            state.listUser = user;
        },
        setAmountUser(state, amount) {
            state.amountUser = amount;
        },
    },
    actions: {
        getListUser: async ({ commit }, params) => {
            interface responseAPI {
                response: getListUser;
                status: boolean;
            }

            interface getListUser {
                data: Array<User>;
                amount: number;
            }

            const result: responseAPI = await $api.get("/users", { params });
            if (result.status) {
                commit("setListUser", result.response.data);
                commit("setAmountUser", result.response.amount);
            }
        },
        createUser: async ({ commit, dispatch }, data) => {
            interface responseAPI {
                response: User;
                status: boolean;
            }

            const result: responseAPI = await $api.post("/users", data);
            return result.status;
        },
        getUserById: async ({ commit }, id) => {
            interface responseAPI {
                response: User;
                status: boolean;
            }

            const result: responseAPI = await $api.get(`/users/${id}`);
            return result;
        },
        deleteUserById: async ({ commit }, id) => {
            interface responseAPI {
                status: boolean;
            }

            const result: responseAPI = await $api.delete(`/users/${id}`);
            return result;
        },
        updateUser: async ({ commit }, { id, data }) => {
            interface responseAPI {
                status: boolean;
            }

            const result: responseAPI = await $api.put(`/users/${id}`, data);
            return result;
        },
    },
    modules: {},
});
